package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DBManager {

    private final String DB_URL;
    private static DBManager instance;
    private static final String SELECT_ALL_USERS = "SELECT * FROM users";
    private static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
    private static final String SELECT_FROM_TEAMS_BY_NAME = "SELECT * FROM teams WHERE name = ?";
    private static final String SELECT_FROM_USERS_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static final String DELETE_FROM_USERS_BY_ID = "DELETE FROM users WHERE id = ?";
    private static final String DELETE_FROM_TEAMS_BY_ID = "DELETE FROM teams WHERE id = ?";
    private static final String INSERT_INTO_USERS = "INSERT INTO users(login) VALUES(?)";
    private static final String INSERT_INTO_TEAMS = "INSERT INTO teams(name) VALUES(?)";
    private static final String INSERT_INTO_USERS_TEAMS = "INSERT INTO users_teams VALUES(?, ?)";
    private static final String UPDATE_TEAMS_BY_ID = "UPDATE teams SET name = ? WHERE id = ?";
    private static final String SELECT_FROM_USER_TEAMS_BY_USER_ID =
            "SELECT t.id, t.name FROM users_teams ut, teams t"
            + "\nWHERE ut.user_id = ? AND ut.team_id = t.id";

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        DB_URL = loadProperties().getProperty("connection.url");
    }

    private Properties loadProperties() {
        var properties = new Properties();
        try (var input = new FileInputStream("app.properties")) {
            properties.load(input);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return properties;
    }

    public List<User> findAllUsers() throws DBException {
        var users = new ArrayList<User>();
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(SELECT_ALL_USERS)) {
            try (var rs = statement.executeQuery()) {
                while (rs.next()) {
                    var user = User.createUser(rs.getString("login"));
                    user.setId(rs.getInt("id"));

                    users.add(user);
                }
            }
            return users;
        } catch (SQLException sqlException) {
            throw new DBException("Shit happens", sqlException);
        }
    }

    public boolean insertUser(User user) throws DBException {
        if (user == null || user.getLogin().isBlank()) return false;

        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(INSERT_INTO_USERS, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getLogin());
            statement.executeUpdate();
            try (var rs = statement.getGeneratedKeys()) {
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                    return true;
                } else {
                    return false;
                }
            }
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        var isDeleted = false;
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(DELETE_FROM_USERS_BY_ID)) {
            for (var user : users) {
                if (user != null && user.getId() > 0) {
                    statement.setInt(1, user.getId());
                    isDeleted |= statement.executeUpdate() == 0;
                }
            }
            return isDeleted;
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public User getUser(String login) throws DBException {
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(SELECT_FROM_USERS_BY_LOGIN)) {
            statement.setString(1, login);
            try (var rs = statement.executeQuery()) {
                if (rs.next()) {
                    var user = User.createUser(rs.getString("login"));
                    user.setId(rs.getInt("id"));
                    return user;
                } else {
                    return null;
                }
            }
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public Team getTeam(String name) throws DBException {
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(SELECT_FROM_TEAMS_BY_NAME)) {
            statement.setString(1, name);
            try (var rs = statement.executeQuery()) {
                if (rs.next()) {
                    var team = Team.createTeam(rs.getString("name"));
                    team.setId(rs.getInt("id"));
                    return team;
                } else {
                    return null;
                }
            }
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        var teams = new ArrayList<Team>();
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(SELECT_ALL_TEAMS)) {
            try (var rs = statement.executeQuery()) {
                while (rs.next()) {
                    var team = Team.createTeam(rs.getString("name"));
                    team.setId(rs.getInt("id"));

                    teams.add(team);
                }
            }
            return teams;
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        if (team == null) return false;

        try (var connection = getDefaultConnection();
             var insertSt = connection.prepareStatement(
                     INSERT_INTO_TEAMS, Statement.RETURN_GENERATED_KEYS)) {
            insertSt.setString(1, team.getName());
            insertSt.executeUpdate();
            try (var rs = insertSt.getGeneratedKeys()) {
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                    return true;
                } else
                    return false;
            }
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null || teams.length == 0) return false;

        var isInserted = false;
        try (Connection connection = getDefaultConnection()) {
            connection.setAutoCommit(false);
            try (var statement = connection.prepareStatement(INSERT_INTO_USERS_TEAMS)) {
                for (var team : teams) {
                    statement.setInt(1, user.getId());
                    if (team != null) {
                        statement.setInt(2, team.getId());
                        isInserted |= statement.executeUpdate() > 0;
                    }
                }
            } catch (SQLException sqlException) {
                connection.rollback();
                throw new DBException(sqlException.getMessage(), sqlException);
            }
            if (isInserted) connection.commit();
            return isInserted;
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        var teams = new ArrayList<Team>();
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(SELECT_FROM_USER_TEAMS_BY_USER_ID)) {
            statement.setInt(1, user.getId());

            try (var rs = statement.executeQuery()) {
                while (rs.next()) {
                    var team = Team.createTeam(rs.getString("name"));
                    team.setId(rs.getInt("id"));

                    teams.add(team);
                }
            }
            return teams;
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(DELETE_FROM_TEAMS_BY_ID)) {
            statement.setInt(1, team.getId());
            return statement.executeUpdate() > 0;
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (var connection = getDefaultConnection();
             var statement = connection.prepareStatement(UPDATE_TEAMS_BY_ID)) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            return statement.executeUpdate() > 0;
        } catch (SQLException sqlException) {
            throw new DBException(sqlException.getMessage(), sqlException);
        }
    }

    private Connection getDefaultConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL);
    }
}