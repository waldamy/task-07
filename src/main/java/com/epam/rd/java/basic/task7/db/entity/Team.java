package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		var team = new Team();
		team.setName(name);
		return team;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Team)
			return Objects.equals(name, ((Team) obj).getName());
		else
			return false;
	}

	@Override
	public String toString() {
		return name;
	}
}
