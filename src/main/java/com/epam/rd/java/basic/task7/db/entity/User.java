package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private User() {}

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		var user = new User();
		user.setLogin(login);
		return user;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User)
			return Objects.equals(login, ((User) obj).getLogin());
		else
			return false;
	}

	@Override
	public String toString() {
		return login;
	}
}
